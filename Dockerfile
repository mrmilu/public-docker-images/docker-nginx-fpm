FROM php:7.4-fpm

ENV rol="nginx-fpm"
LABEL mantainer="asanz@mrmilu.com"
LABEL version="php74-composer2-nginx18"

# Install base requirements
RUN apt update && \
    apt install --no-install-recommends -y \
    cron \
    git \
    gzip \
    libbz2-dev \
    libcurl4-openssl-dev \
    libfreetype6-dev \
    libmagickwand-dev \
    libicu-dev \
    libjpeg62-turbo-dev \
    libmcrypt-dev \
    libonig-dev \
    libpng-dev \
    libjpeg-dev \
    libpq-dev \
    libwebp-dev \
    libsodium-dev \
    libssh2-1-dev \
    libxslt1-dev \
    libzip-dev \
    lsof \
    default-mysql-client \
    vim \
    zip \
    unzip \
    procps \
    gettext-base \
    curl \
    # Nginx deps
    build-essential \
    libgd-dev \
    libpcre3 \
    libpcre3-dev \ 
    zlib1g-dev \
    geoip-database \
    libperl-dev \
    libgeoip-dev \
    libssl-dev 

RUN pecl channel-update pecl.php.net && \
  pecl install -o -f imagick xdebug redis && rm -rf /tmp/pear && docker-php-ext-enable redis imagick xdebug

RUN docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp

RUN docker-php-ext-install \
  curl \
  bcmath \
  bz2 \
  calendar \
  exif \
  gd \
  gettext \
  intl \
  mbstring \
  mysqli \
  opcache \
  pcntl \
  pdo_mysql \
  soap \
  sockets \
  sodium \
  sysvmsg \
  sysvsem \
  sysvshm \
  xsl \
  zip

## Nginx https://docs.nginx.com/nginx/admin-guide/installing-nginx/installing-nginx-open-source/#sources
RUN cd /tmp && curl -LO http://nginx.org/download/nginx-1.18.0.tar.gz && \
    tar zxf nginx-1.18.0.tar.gz && \
    cd nginx-1.18.0 && \
    sed -i 's/define NGINX_VER          "nginx\/" NGINX_VERSION/define NGINX_VER          " ಥ╭╮ಥ "/' src/core/nginx.h && \
    sed -i 's/<hr><center>nginx<\/center>/<hr><center>" NGINX_VER "<\/center>/' src/http/ngx_http_special_response.c && \
    git clone -b master https://github.com/kaltura/nginx-aws-auth-module.git && \
    git clone -b master https://github.com/openresty/headers-more-nginx-module.git && \
    git clone -b master https://github.com/vozlt/nginx-module-vts.git && \
    ./configure --user=www-data --group=www-data --prefix=/etc/nginx --sbin-path=/usr/local/bin/ --pid-path=/tmp/nginx.pid --conf-path=/etc/nginx/nginx.conf \
      --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log \
      --with-compat \
      --with-stream \
      --with-debug \
      --with-file-aio \
      --with-threads \
      --with-stream_ssl_module \
      --with-stream \
      --with-mail=dynamic \
      --with-http_ssl_module \
      --with-http_realip_module \
      --with-http_addition_module \
      --with-http_sub_module \
      --with-http_dav_module \
      --with-http_flv_module \
      --with-http_mp4_module \
      --with-http_gunzip_module \
      --with-http_gzip_static_module \
      --with-http_random_index_module \
      --with-http_secure_link_module \
      --with-http_stub_status_module \
      --with-http_auth_request_module \
      --with-http_image_filter_module=dynamic \
      --with-http_geoip_module=dynamic \
      --with-http_perl_module=dynamic \
      --with-stream_ssl_module \
      --with-stream_ssl_preread_module \
      --with-stream_realip_module \
      --with-stream_geoip_module=dynamic \
      --with-http_slice_module \
      --with-mail \
      --with-mail_ssl_module \
      --with-compat \
      --with-file-aio \
      --with-http_v2_module \
      --add-module=nginx-aws-auth-module \
      --add-module=headers-more-nginx-module/ \
      --add-module=nginx-module-vts && \
      make && make install  && \
    cd /tmp && \
    mkdir /etc/nginx/conf.d && \
    rm -f nginx-1.18.0.tar.gz && \
    rm -rf nginx-1.18.0 && apt-get autoremove -y

RUN apt install -y supervisor && mkdir -p /var/log/supervisor

RUN curl https://ssl-config.mozilla.org/ffdhe2048.txt > /etc/nginx/dhparam

RUN useradd -u 1000 -g www-data linux && useradd -u 501 -g www-data  mac

COPY supervisord/supervisord.conf /etc/supervisord.conf

RUN chown -R www-data:www-data /etc/nginx /var/log/nginx && chmod -R ug+w /var/log/nginx /etc/nginx

# Install composer
RUN curl -sS https://getcomposer.org/installer | \
    php -- --version=2.0.8 --install-dir=/usr/local/bin --filename=composer 

WORKDIR /src/

ENTRYPOINT [ "/usr/bin/supervisord", "-n", "-c", "/etc/supervisord.conf" ]